<?php

require 'vendor/autoload.php';

use League\Flysystem\Filesystem;
use League\Flysystem\Adapter\Local;
use League\Flysystem\Plugin\ForcedCopy;
use League\Flysystem\FileNotFoundException;

$adapter = new Local(__DIR__.'/files');
$fs = new Filesystem($adapter);

/**
 * 方法一 (建議)
 */
$forceCopy = new ForcedCopy();
$forceCopy->setFilesystem($fs);
$response = $forceCopy->handle('aaa', 'backup/aaa');

/**
 * 方法二
 */
// try {
//     $deleted = $fs->delete('backup/aaa');
// } catch (FileNotFoundException $e) {
//     $deleted = true;
// }
// if ($deleted) {
//     $fs->copy('aaa', 'backup/aaa');
// }
